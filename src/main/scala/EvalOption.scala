import cats.Eval
import cats.data.OptionT

object EvalOption {

  type EvalOption[A] = OptionT[Eval, A]

  def fromOption[A](maybe: Option[A]): EvalOption[A] =
    OptionT.fromOption[Eval](maybe)
  def pure[A](a: A): EvalOption[A] = OptionT.pure(a)
  def none[A]: EvalOption[A] = OptionT.none
  def later[A](a: => A): EvalOption[A] =
    OptionT.liftF {
      Eval.later(a)
    }

}
