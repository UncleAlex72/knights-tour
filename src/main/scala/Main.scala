import java.time.{Duration, Instant}

import EvalOption.EvalOption

import scala.io.Source
import scala.util.Using

object Main extends App {

  val board: Map[Cell, Int] =
    Using(
      Source
        .fromFile(this.getClass.getClassLoader.getResource("tour1.txt").toURI)
    ) { source =>
      val cells = for {
        (line, row) <- source.getLines().toSeq.zipWithIndex
        (cell, column) <- line.toSeq.sliding(2, 2).map(_.unwrap).zipWithIndex
        if cell != ".."
      } yield {
        val value = Integer.parseInt(cell)
        val number = if (value == 0) 100 else value
        Cell(column, row) -> number
      }
      cells.toMap
    }.get

  val startingCell = board.find(_._2 == 1).map(_._1).get
  val tour = Tour(board, board, startingCell)

  val start = Instant.now()
  tour.solve().value.value match {
    case None           => println("No solution")
    case Some(solution) => println(solution)
  }
  val finish = Instant.now()
  val elapsedMillis: Long = Duration.between(start, finish).toMillis
  println(s"Algorithm took ${elapsedMillis}ms")

  case class Cell(column: Int, row: Int) {
    def move(x: Int, y: Int): Cell = Cell(column + x, row + y)
  }

  case class Tour(
      initialBoard: Map[Cell, Int],
      board: Map[Cell, Int],
      currentCell: Cell
  ) {

    val SIZE = 10

    def solve(): EvalOption[Tour] = {
      val maybeCurrentNumber = EvalOption.fromOption(board.get(currentCell))
      maybeCurrentNumber.flatMap { number =>
        if (number == SIZE * SIZE) {
          EvalOption.pure(this)
        } else {
          val nextCells = nextPossibleCells()
          val nextNumber = number + 1
          val maybeNextCell = board.find(_._2 == nextNumber).map(_._1)
          maybeNextCell match {
            case Some(nextCell) =>
              if (nextCells.contains(nextCell)) {
                this.copy(currentCell = nextCell).solve()
              } else {
                EvalOption.none
              }
            case None =>
              val possibleNextCells =
                nextCells.filter(nextCell => !board.contains(nextCell))
              possibleNextCells.foldLeft(EvalOption.none[Tour]) {
                (acc, nextCell) =>
                  val eoNextTour = EvalOption.later(
                    this
                      .copy(
                        board = board + (nextCell -> nextNumber),
                        currentCell = nextCell
                      )
                  )
                  acc.orElse(eoNextTour.flatMap(_.solve()))
              }
          }
        }
      }
    }

    override def toString: String = {
      import io.AnsiColor._
      def rowToString(row: Int): String = {
        val cells = Range(0, SIZE).map { column =>
          val cell = Cell(column, row)
          val uncolouredCell = board
            .get(cell)
            .map(_ % 100)
            .map(n => f"$n%02d")
            .getOrElse("..")
          if (cell == currentCell) {
            s"$REVERSED$GREEN$uncolouredCell$RESET"
          } else if (initialBoard.contains(cell)) {
            uncolouredCell
          } else {
            s"$REVERSED$BLUE$uncolouredCell$RESET"
          }
        }
        cells.mkString(" ")
      }
      Range(0, SIZE).map(rowToString).mkString("\n")
    }

    private def nextPossibleCells(): LazyList[Cell] = {
      val nextDeltas = LazyList(
        (-1, -2),
        (-1, 2),
        (1, -2),
        (1, 2),
        (-2, -1),
        (-2, 1),
        (2, -1),
        (2, 1)
      )
      nextDeltas.map(d => currentCell.move(d._1, d._2)).filter { cell =>
        cell.column >= 0 && cell.column < SIZE && cell.row >= 0 && cell.row < SIZE
      }
    }
  }

}
